# Indonesian translation of accountservice
# Copyright (C) 2011 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the accountservice package.
#
# Translators:
# Andika Triwidada <andika@gmail.com>, 2011, 2020
msgid ""
msgstr ""
"Project-Id-Version: accountsservice master\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/accountsservice/"
"accountsservice/issues\n"
"POT-Creation-Date: 2020-03-30 06:50+0000\n"
"PO-Revision-Date: 2020-03-30 18:46+0700\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Indonesian (http://www.transifex.com/freedesktop/"
"accountsservice/language/id/)\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.3\n"

#: data/org.freedesktop.accounts.policy.in:11
msgid "Change your own user data"
msgstr "Ubah data penggunamu sendiri"

#: data/org.freedesktop.accounts.policy.in:12
msgid "Authentication is required to change your own user data"
msgstr ""
"Autentikasi diperlukan untuk mengubah data pengguna milikmu sendiri"

#: data/org.freedesktop.accounts.policy.in:21
msgid "Change your own user password"
msgstr "Ubah kata sandi penggunamu sendiri"

#: data/org.freedesktop.accounts.policy.in:22
msgid "Authentication is required to change your own user password"
msgstr ""
"Autentikasi diperlukan untuk mengubah kata sandi pengguna milikmu sendiri"

#: data/org.freedesktop.accounts.policy.in:31
msgid "Manage user accounts"
msgstr "Kelola akun pengguna"

#: data/org.freedesktop.accounts.policy.in:32
msgid "Authentication is required to change user data"
msgstr "Autentikasi diperlukan untuk mengubah data pengguna"

#: data/org.freedesktop.accounts.policy.in:41
msgid "Change the login screen configuration"
msgstr "Ubah konfigurasi layar log masuk"

#: data/org.freedesktop.accounts.policy.in:42
msgid "Authentication is required to change the login screen configuration"
msgstr "Autentikasi diperlukan untuk mengubah konfigurasi layar log masuk"

#: src/main.c:201
msgid "Output version information and exit"
msgstr "Tampilkan informasi versi dan keluar"

#: src/main.c:202
msgid "Replace existing instance"
msgstr "Ganti instansi yang ada"

#: src/main.c:203
msgid "Enable debugging code"
msgstr "Fungsikan kode pengawakutuan"

#: src/main.c:222
msgid ""
"Provides D-Bus interfaces for querying and manipulating\n"
"user account information."
msgstr ""
"Menyediakan antar muka D-Bus untuk menanyakan dan memanipulasi\n"
"informasi akun pengguna"
